<?php
/**
 * Created by PhpStorm.
 * User: tanlinh
 * Date: 2/26/2016
 * Time: 10:09 PM
 */
return [
    'dashboard' => 'Dashboard',
    'media_zone' => 'Media Zone',
    'fixture' => 'Fixture',
    'article' => 'Article',
    'list_articles' => 'List Articles',
    'list_live' => 'List Live',
    'file_manager' => 'File Manager',
    'sms_zone' => 'SMS Zone',
    'report_zone' => 'Report Zone',
    'search_zone' => 'Search Zone',
    'media_report' => 'Media Report',
    'sms_report' => 'SMS Report',
    'user' => 'User',
    'profile' => 'My Profile',
    'list_user' => 'List Users',
    'list_roles' => 'List Roles',
    'list_permissions' => 'List Permissions',
    'settings' => 'Settings',
    'general_setting' => 'General Setting',
    'system_setting' => 'System Setting',
    'main_navi' => 'MAIN NAVIGATION',
    'main_index' => 'ARTICLES INDEX',
    'unapproved' => 'Unapproved Articles',
    'create_article' => 'Create Article',
    'edit_article' => 'Edit Article',
    'cat_title' => 'Chuyên mục',
    'cat_add' => 'Thêm chuyên mục',
    'cat_list' => 'Danh sách chuyên mục',
	'create_article' => 'Create Article',
    'edit_article' => 'Edit Article',
    'create_recipe' => 'Create Recipe',
    'edit_recipe' => 'Edit Recipe',
];