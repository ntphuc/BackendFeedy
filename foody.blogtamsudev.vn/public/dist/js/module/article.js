$('#article_form').submit(function (event) {
    event.preventDefault();
    $categories = [];
    $location = [];
    $guess = [];

    $array = $('input[name="category[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $categories.push($(this).val());
    });
    $array = $('input[name="location[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $location.push($(this).val());
    });
    $array = $('input[name="guess[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $guess.push($(this).val());
    });
    var formData = {
        _token: $("input[name='_token']").val(),
        type: 'Review',
        content: $("textarea[name='content']").val(),
        status: $('select[name="status"]').val() === '' ? '' : $('select[name="status"]').val(),
        title: $("input[name='title']").val() === '' ? swal({
            title: 'Chưa nhập tiêu đề',
            type: 'error'
        }) : $("input[name='title']").val(),
        address: $("input[name='address']").val() === '' ? swal({
            title: 'Chưa nhập địa chỉ quán',
            type: 'error'
        }) : $("input[name='address']").val(),
        price: $("input[name='price']").val() === '' ? swal({
            title: 'Chưa nhập giá',
            type: 'error'
        }) : $("input[name='price']").val(),
        time_action: $("input[name='time_action']").val() === '' ? swal({
            title: 'Chưa nhập thời gian hoạt động',
            type: 'error'
        }) : $("input[name='time_action']").val(),
        description: $("textarea[name='description']").val() === '' ? swal({
            title: 'Chưa nhập mô tả',
            type: 'error'
        }) : $("textarea[name='description']").val(),
        tags: $("input[name='tags']").val() === '' ? swal({
            title: 'Chưa nhập tags',
            type: 'error'
        }) : $("input[name='tags']").val(),
        related: $("input[name='related']").val() === '' ? swal({
            title: 'Chưa nhập bài liên quan',
            type: 'error'
        }) : $("input[name='related']").val(),
        category: $categories.length === 0 ? swal({
            title: 'Chưa chọn chuyên mục',
            type: 'error'
        }) : JSON.stringify($categories),
        location: $location.length === 0 ? swal({
            title: 'Chưa chọn địa điểm',
            type: 'error'
        }) : JSON.stringify($location),
        guess: $guess.length === 0 ? swal({
            title: 'Chưa chọn đồ ăn',
            type: 'error'
        }) : JSON.stringify($guess),
        thumbnail: $("input[name='thumbnail']").val() === '' ? swal({
            title: 'Chưa chọn ảnh đại diện',
            type: 'error'
        }) : $("input[name='thumbnail']").val(),
        gallery: $("input[name='gallery']").val() === '' ? swal({
            title: 'Chưa chọn gallery',
            type: 'error'
        }) : $("input[name='gallery']").val(),
        seo_title: $("input[name='seo_title']").val() === '' ? $("input[name='title']").val() : $("input[name='seo_title']").val(),
        seo_meta: $("input[name='seo_meta']").val() === '' ? $("input[name='title']").val() : $("input[name='seo_meta']").val(),
        seo_description: $("textarea[name='seo_description']").val() === '' ? $("textarea[name='description']").val() : $("textarea[name='seo_description']").val(),
    };
    if (!formData.title || !formData.category || !formData.address || !formData.price || !formData.description || !formData.thumbnail || !formData.related) {
        return;
    }
    $.ajax({
        type: "POST",
        url: '/media/article/create/',
        dataType: 'json',
        data: formData,
        success: function (res) {
            event.preventDefault();
            swal({title: res.msg, type: res.status}, function (isConfirm) {
                if (isConfirm) {
                    location.reload();
                }
            });
        }
    }
    );
});

$('#recipe_form').submit(function (event) {
    event.preventDefault();
    $categories = [];
    $location = [];
    $array = $('input[name="category[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $categories.push($(this).val());
    });
    $array = $('input[name="location[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $location.push($(this).val());
    });
    var formData = {
        _token: $("input[name='_token']").val(),
        type: 'Recipe',
        status: $('select[name="status"]').val() === '' ? '' : $('select[name="status"]').val(),
        content: $("textarea[name='content']").val(),
        title: $("input[name='title']").val() === '' ? swal({
            title: 'Chưa nhập tiêu đề',
            type: 'error'
        }) : $("input[name='title']").val(),
        prep_time: $("input[name='prep_time']").val() === '' ? swal({
            title: 'Chưa nhập thời gian chuẩn bị',
            type: 'error'
        }) : $("input[name='prep_time']").val(),
        cook_time: $("input[name='cook_time']").val() === '' ? swal({
            title: 'Chưa nhập thời gian nấu',
            type: 'error'
        }) : $("input[name='cook_time']").val(),
        ingredients: $("textarea[name='ingredients']").val() === '' ? swal({
            title: 'Chưa nhập nguyên liệu',
            type: 'error'
        }) : $("textarea[name='ingredients']").val(),
        directions: $("textarea[name='directions']").val() === '' ? swal({
            title: 'Chưa nhập nguyên liệu',
            type: 'error'
        }) : $("textarea[name='directions']").val(),
        description: $("textarea[name='description']").val() === '' ? swal({
            title: 'Chưa nhập mô tả',
            type: 'error'
        }) : $("textarea[name='description']").val(),
        tags: $("input[name='tags']").val() === '' ? swal({
            title: 'Chưa nhập tags',
            type: 'error'
        }) : $("input[name='tags']").val(),
        related: $("input[name='related']").val() === '' ? swal({
            title: 'Chưa nhập bài liên quan',
            type: 'error'
        }) : $("input[name='related']").val(),
        category: $categories.length === 0 ? swal({
            title: 'Chưa chọn chuyên mục',
            type: 'error'
        }) : JSON.stringify($categories),
        location: $location.length === 0 ? swal({
            title: 'Chưa chọn địa điểm',
            type: 'error'
        }) : JSON.stringify($location),
        thumbnail: $("input[name='thumbnail']").val() === '' ? swal({
            title: 'Chưa chọn ảnh đại diện',
            type: 'error'
        }) : $("input[name='thumbnail']").val(),
        gallery: $("input[name='gallery']").val() === '' ? swal({
            title: 'Chưa chọn gallery',
            type: 'error'
        }) : $("input[name='gallery']").val(),
        seo_title: $("input[name='seo_title']").val() === '' ? $("input[name='title']").val() : $("input[name='seo_title']").val(),
        seo_meta: $("input[name='seo_meta']").val() === '' ? $("input[name='title']").val() : $("input[name='seo_meta']").val(),
        seo_description: $("textarea[name='seo_description']").val() === '' ? $("textarea[name='description']").val() : $("textarea[name='seo_description']").val(),
    };
    if (!formData.title || !formData.prep_time || !formData.cook_time || !formData.directions || !formData.ingredients || !formData.category || !formData.description || !formData.thumbnail || !formData.related) {
        return;
    }
    $.ajax({
        type: "POST",
        url: '/media/recipe/create/',
        dataType: 'json',
        data: formData,
        success: function (res) {
            event.preventDefault();
            swal({title: res.msg, type: res.status}, function (isConfirm) {
                if (isConfirm) {
                    location.reload();
                }
            });
        }
    }
    );
});

$('#article_form_edit').submit(function (event) {
    event.preventDefault();
    $categories = [];
    $location = [];
    $guess = [];
    $array = $('input[name="category[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $categories.push($(this).val());
    });
    $array = $('input[name="location[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $location.push($(this).val());
    });
    $array = $('input[name="guess[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $guess.push($(this).val());
    });
    var id = $('input[name=id]').val() === '' ? null : $('input[name=id]').val();

    var formData = {
        _token: $("input[name='_token']").val(),
        id: $('input[name=id]').val() === '' ? null : $('input[name=id]').val(),
        type: 'Review',
        content: $("textarea[name='content']").val(),
        title: $("input[name='title']").val() === '' ? swal({
            title: 'Chưa nhập tiêu đề',
            type: 'error'
        }) : $("input[name='title']").val(),
        address: $("input[name='address']").val() === '' ? swal({
            title: 'Chưa nhập địa chỉ quán',
            type: 'error'
        }) : $("input[name='address']").val(),
        price: $("input[name='price']").val() === '' ? swal({
            title: 'Chưa nhập giá',
            type: 'error'
        }) : $("input[name='price']").val(),
        time_action: $("input[name='time_action']").val() === '' ? swal({
            title: 'Chưa nhập thời gian hoạt động',
            type: 'error'
        }) : $("input[name='time_action']").val(),
        description: $("textarea[name='description']").val() === '' ? swal({
            title: 'Chưa nhập mô tả',
            type: 'error'
        }) : $("textarea[name='description']").val(),
        tags: $("input[name='tags']").val() === '' ? swal({
            title: 'Chưa nhập tags',
            type: 'error'
        }) : $("input[name='tags']").val(),
        related: $("input[name='related']").val() === '' ? swal({
            title: 'Chưa nhập bài liên quan',
            type: 'error'
        }) : $("input[name='related']").val(),
        category: $categories.length === 0 ? swal({
            title: 'Chưa chọn chuyên mục',
            type: 'error'
        }) : JSON.stringify($categories),
        location: $location.length === 0 ? swal({
            title: 'Chưa chọn địa điểm',
            type: 'error'
        }) : JSON.stringify($location),
        guess: $guess.length === 0 ? swal({
            title: 'Chưa chọn đồ ăn',
            type: 'error'
        }) : JSON.stringify($guess),
        thumbnail: $("input[name='thumbnail']").val() === '' ? swal({
            title: 'Chưa chọn ảnh đại diện',
            type: 'error'
        }) : $("input[name='thumbnail']").val(),
        gallery: $("input[name='gallery']").val(),
        seo_title: $("input[name='seo_title']").val() === '' ? $("input[name='title']").val() : $("input[name='seo_title']").val(),
        seo_meta: $("input[name='seo_meta']").val() === '' ? $("input[name='title']").val() : $("input[name='seo_meta']").val(),
        seo_description: $("textarea[name='seo_description']").val() === '' ? $("textarea[name='description']").val() : $("textarea[name='seo_description']").val(),
    };
    if (!formData.title || !formData.category || !formData.address || !formData.price || !formData.description || !formData.thumbnail || !formData.related) {
        return;
    }

    $.ajax({
        type: "POST",
        url: '/media/article/edit/' + id,
        dataType: 'json',
        data: formData,
        success: function (res) {
            event.preventDefault();
            swal({title: res.msg, type: res.status}, function (isConfirm) {
                if (isConfirm) {
                    location.reload();
                }
            });
        }
    }
    );
});

$('#recipe_form_edit').submit(function (event) {
    event.preventDefault();
    $categories = [];
    $location = [];
    $array = $('input[name="category[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $categories.push($(this).val());
    });
    $array = $('input[name="location[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            $location.push($(this).val());
    });
    var id = $('input[name=id]').val() === '' ? null : $('input[name=id]').val();

    var formData = {
        _token: $("input[name='_token']").val(),
        id: $('input[name=id]').val() === '' ? null : $('input[name=id]').val(),
        type: 'Recipe',
        status: $('select[name="status"]').val() === '' ? '' : $('select[name="status"]').val(),
        content: $("textarea[name='content']").val(),
        title: $("input[name='title']").val() === '' ? swal({
            title: 'Chưa nhập tiêu đề',
            type: 'error'
        }) : $("input[name='title']").val(),
        prep_time: $("input[name='prep_time']").val() === '' ? swal({
            title: 'Chưa nhập thời gian chuẩn bị',
            type: 'error'
        }) : $("input[name='prep_time']").val(),
        cook_time: $("input[name='cook_time']").val() === '' ? swal({
            title: 'Chưa nhập thời gian nấu',
            type: 'error'
        }) : $("input[name='cook_time']").val(),
        ingredients: $("textarea[name='ingredients']").val() === '' ? swal({
            title: 'Chưa nhập nguyên liệu',
            type: 'error'
        }) : $("textarea[name='ingredients']").val(),
        directions: $("textarea[name='directions']").val() === '' ? swal({
            title: 'Chưa nhập nguyên liệu',
            type: 'error'
        }) : $("textarea[name='directions']").val(),
        description: $("textarea[name='description']").val() === '' ? swal({
            title: 'Chưa nhập mô tả',
            type: 'error'
        }) : $("textarea[name='description']").val(),
        tags: $("input[name='tags']").val() === '' ? swal({
            title: 'Chưa nhập tags',
            type: 'error'
        }) : $("input[name='tags']").val(),
        related: $("input[name='related']").val() === '' ? swal({
            title: 'Chưa nhập bài liên quan',
            type: 'error'
        }) : $("input[name='related']").val(),
        category: $categories.length === 0 ? swal({
            title: 'Chưa chọn chuyên mục',
            type: 'error'
        }) : JSON.stringify($categories),
        location: $location.length === 0 ? swal({
            title: 'Chưa chọn địa điểm',
            type: 'error'
        }) : JSON.stringify($location),
        thumbnail: $("input[name='thumbnail']").val() === '' ? swal({
            title: 'Chưa chọn ảnh đại diện',
            type: 'error'
        }) : $("input[name='thumbnail']").val(),
        gallery: $("input[name='gallery']").val(),
        seo_title: $("input[name='seo_title']").val() === '' ? $("input[name='title']").val() : $("input[name='seo_title']").val(),
        seo_meta: $("input[name='seo_meta']").val() === '' ? $("input[name='title']").val() : $("input[name='seo_meta']").val(),
        seo_description: $("textarea[name='seo_description']").val() === '' ? $("textarea[name='description']").val() : $("textarea[name='seo_description']").val(),
    };

    if (!formData.title || !formData.prep_time || !formData.cook_time || !formData.directions || !formData.ingredients || !formData.category || !formData.description || !formData.thumbnail || !formData.related) {
        return;
    }

    $.ajax({
        type: "POST",
        url: '/media/recipe/edit/' + id,
        dataType: 'json',
        data: formData,
        success: function (res) {
            event.preventDefault();
            swal({title: res.msg, type: res.status}, function (isConfirm) {
                if (isConfirm) {
                    location.reload();
                }
            });
        }
    }
    );
});

function deleteArticle(target) {
    var article_id = $(target).data('article_id');
    return swal({
        title: AdminCPLang.lang_1,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: AdminCPLang.lang_3,
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: "POST",
            url: '/media/article/delete',
            dataType: 'json',
            data: {id: article_id, select_action: 'Delete', type: 'ajax'}, // serializes the form's elements.
            success: function (res) {
                swal({title: res.msg, type: res.status});
                $('tr.post-item.post-id-' + article_id).remove();
            },
            error: function (resp) {
                alert('Erorr!');
            }
        }
        );
    });
}

// review Article
function reviewArticle(target) {
    var article_id = $(target).data('article_id');
    loadModalContent('reviewArticleModalBody', '/media/article/review/' + article_id)
    $("#reviewArticleModal").modal('show');
}

// mo cua so chon list to web publish
$("#btn-status").on('click', function () {
    var status = $(this).attr('status');
    if (status == "off") {
        $(".modal-body #webPublish ").slideDown(300);
        $(".modal-body #viewReviewArticle ").slideUp(300);
        $(this).html('Back');
        $(this).attr('status', 'on');
    }
    if (status == "on") {
        $(".modal-body #webPublish ").slideUp(300);
        $(".modal-body #viewReviewArticle ").slideDown(300);
        $(this).html('Publish');
        $(this).attr('status', 'off');
    }
});
$('.modal-content .modal-header #close').click(function () {
    // click vao nut dong cua so thi dong bang publish
    $(".modal-body #webPublish ").slideDown(300);
    $(".modal-body #viewReviewArticle ").slideUp(300);
    $('.modal-content .modal-header #btn-status').html('Publish');
    $('.modal-content .modal-header #btn-status').attr('status', 'off');
});
$("html").click(function (e) {                          // click ra ngoai dong cua so thi dong bang publish
    if ($('.modal-content').is(":visible")) {
    } else {
        $('.modal-body #webPublish').slideDown(300);
        $('.modal-body #viewReviewArticle').slideUp(300);
        $('.modal-content .modal-header #btn-status').html('Publish');
        $('.modal-content .modal-header #btn-status').attr('status', 'off');
    }
});

// submit to list web publish
function summitToWebPublish(target) {
    var article_id = $(target).data('article_id');
    var type_to_publish = $(target).data('type');
    var list = [];
    $array = $('input[name="listWeb[]"]:checked').each(function ()
    {
        if ($(this).is(':checked'))
            list.push($(this).val());
    });
    if (list == '') {
        alert("Error ! Choose Web Fail !");
    } else {
        $.ajax({
            type: "POST",
            url: '/media/article/submit',
            dataType: 'json',
            data: {id: article_id, st: type_to_publish, li: list},
            success: function (res) {
                if (res.status == "error") {
                    swal({title: res.msg, type: res.status});
                } else {
                    $('#reviewArticleModal').modal('hide');
                    swal({title: res.msg, type: res.status});
                    location.reload();
                }
            },
            error: function (resp) {
                alert('Erorr!');
            }
        }
        );
    }

}

// Active status Article
function activeArticle(target) {
    var article_id = $(target).data('article_id');
    var st = $(target).data('status');
    $.ajax({
        type: "POST",
        url: '/media/article/update-status',
        dataType: 'json',
        data: {id: article_id, st: st}, // serializes the form's elements.
        success: function (res) {
            $('#reviewArticleModal').modal('hide');
            swal({title: res.msg, type: res.status});
            setTimeout(function () {
                window.location.reload(1);
            }, 2000);
        },
        error: function (resp) {
            alert('Erorr!');
        }
    }
    );
}
