<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', 'Auth\AuthController@getLogin');
Route::post('/', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('home', 'HomeController@getIndex');
    Route::controller('user', 'UserController');

    Route::group(['prefix' => 'media'], function () {
        Route::controller('article', 'ArticleController', [
            'postCreate' => 'createArticle', 'postEdit' => 'editArticle',
        ]);
        Route::controller('location', 'LocationController');
        Route::controller('category', 'CategoryController');
        Route::controller('food', 'FoodController');
        Route::controller('recipe', 'RecipeController');
        Route::controller('edit/{$id}', 'TraitController\ArticleDetail');
        Route::controller('delete/{$id}', 'TraitController\ArticleDetail');
        Route::controller('delete/{$id}', 'RecipeController');
        Route::controller('publish/{$id}', 'TraitController\ArticleDetail');
        Route::controller('publish/{$id}', 'RecipeController');
        Route::controller('built-top', 'BuilttopController');
    });
});
