
<script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!--<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>-->
<script src="{{ asset('dist/js/jquery-ui.js') }}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
<script src="{{ asset('dist/js/app.min.js') }}"></script>
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('plugins/imageloader/jquery.imageloader.js') }}"></script>
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('dist/js/admin.js') }}"></script>
<script src="{{ asset('dist/js/caret.js') }}"></script>
<script src="{{ asset('dist/js/tag.js') }}"></script>

<script>
    AdminCPLang = {
        @foreach(trans('common') as $key => $value)
        '{{$key}}': '{{ $value }}',
        @endforeach
    }
    $(".select2").select2();

</script>