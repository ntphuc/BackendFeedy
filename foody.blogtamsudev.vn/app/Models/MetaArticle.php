<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaArticle extends Model
{
    protected $table = 'meta_article' ;
}
