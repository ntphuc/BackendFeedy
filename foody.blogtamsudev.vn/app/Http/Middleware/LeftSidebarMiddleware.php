<?php

namespace App\Http\Middleware;

use Auth;
use Menu;
use Closure;
use Illuminate\Support\Facades\Lang;

class LeftSidebarMiddleware
{
    public $articles_unapprove;

    public function __construct()
    {
        $this->articles_unapprove = 20;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $index_1st = $this->articles_unapprove;
        if (Auth::check()) {
            $user = auth()->user();
            Menu::make('left_navbar', function ($menu) use ($index_1st, $user) {
                $menu->style('navigation');

                if (in_array($user->user_type, [config('admincp.user_type.type_1'), config('admincp.user_type.type_2')])) {
                    $menu->add([
                        'title' => trans('menu.main_index'),
                        'header' => true
                    ]);

                    $menu->add([
                        'title' => trans('menu.unapproved') . '<small class="label pull-right bg-blue">' . $index_1st . '</small>',
                        'url' => '/',
                        'icon' => 'fa fa-circle-o text-red'
                    ]);
                }

                $menu->add([
                    'title' => trans('menu.main_navi'),
                    'header' => true
                ]);
                $menu->add([
                    'url' => '/home',
                    'title' => trans('menu.dashboard'),
                    'icon' => 'fa fa-desktop'
                ]);

                if (in_array($user->user_type, [config('admincp.user_type.type_1'), config('admincp.user_type.type_2'), config('admincp.user_type.type_3')])) {
                    $menu->dropdown(trans('menu.media_zone'), function ($sub) {
                        $sub->url('/media/article', 'Bài Viết', ['icon' => 'fa fa-circle-o']);
                        $sub->url('/media/category', 'Chuyên Mục', ['icon' => 'fa fa-circle-o']);
                        $sub->url('/media/location', 'Địa Điểm', ['icon' => 'fa fa-circle-o']);
                        $sub->url('/media/built-top', 'Bài Built Top', ['icon' => 'fa fa-circle-o']);
                        $sub->url('/filemanager/index.html', 'Quản Lý Album', ['icon' => 'fa fa-circle-o']);
                    }, '', ['icon' => 'fa fa-film', 'parent_url' => 'media']);

                }

                $menu->dropdown(trans('menu.user'), function ($sub) {
                    $sub->url('/user/profile', trans('menu.profile'), ['icon' => 'fa fa-circle-o']);
                    $sub->url('/user', trans('menu.list_user'), ['icon' => 'fa fa-circle-o']);
                }, '', ['icon' => 'glyphicon glyphicon-user', 'parent_url' => 'user']);

            });
        }

        return $next($request);
    }
}
