@extends('layouts.master')

@section('main_content')
    <section class="content-header">
        <h1>{{ trans('menu.sms_report') }}</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{ trans('menu.report_zone') }}</li>
            <li class="active">{{ trans('menu.sms_report') }}</li>
        </ol>
    </section>

    <section class="content">

        @if(request('start_date') != '' && request('end_date') != '' & request('shortcode') != '')
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Info!</h4>
                <h4 class="text-red">{{ trans('report.info_alert') }} {{ Cache::get(request('shortcode') . '_' . strtotime(request('start_date')) . '_' . strtotime(request('end_date'))) }}
                    UTC</h4>
            </div>
        @endif

        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-bar-chart"></i>
                <h3 class="box-title">{{ trans('report.service_overview') }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div style="padding-bottom: 5px; margin-bottom: 5px; border-bottom: 1px solid #ddd">
                    <form method="get" action="/report/sms" autocomplete="off" role="form" class="form-inline"
                          id="smsFormReport">
                        <div class="form-group">
                            <div id="reportrange" class="btn btn-default "
                                 style="position: relative; display: inline-block">
                                <i class="glyphicon glyphicon-calendar"></i>
                                <span id="time_select">
                                        {{ trans('report.choose_date_range') }}
                                </span> <b class="caret"></b>
                                <input type="hidden" name="start_date" id="start_date"
                                       @if(request('start_date') != '')value="{{ request('start_date') }}"@endif>
                                <input type="hidden" name="end_date" id="end_date"
                                       @if(request('end_date') != '')value="{{ request('end_date') }}"@endif>
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="shortcode" id="short_code" data-live-search="true"
                                    data-width="120px">
                                <option value="">{{ trans('report.short_code') }}</option>
                                @foreach(auth()->user()->report_zone as $short_code)
                                    <option value="{{ $short_code }}"
                                            @if(request('shortcode') != '' && request('shortcode') == $short_code) selected @endif >{{ $short_code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" class="btn btn-primary" name="search" value="{{ trans('home.search') }}">
                    </form>
                </div>

                <div class="statitics">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{ number_format($total_num['num_register']) }}
                                        /{{ number_format($total_num['num_restore']) }}</h3>
                                    <p>{{ trans('report.num_register') }}/{{ trans('report.num_restore') }}</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user-plus"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>{{ number_format($total_num['num_cancellation']) }}</h3>
                                    <p>{{ trans('report.num_cancel') }}</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user-times"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{ number_format($total_num['num_subscriber']) }}</h3>
                                    <p>{{ trans('report.total_subscriber') }}</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-bar-chart"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow-gradient">
                                <div class="inner">
                                    <h3>{{ number_format($total_num['num_unique']) }}</h3>
                                    <p>{{ trans('report.unique_subscriber') }}</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cubes"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                    <h4><i class="fa fa-bar-chart"></i> {{ trans('report.revenue') }}</h4>
                    @if(!empty($revenue))
                            <!-- doanh thu theo bieu do -->
                    <div class="col-md-12 chart">
                        <div class="pull-right">
                            <button class="btn btn-sm btn-success">= MONFEE</button>
                            <button class="btn btn-sm">= REGISTER DAY</button>
                        </div>
                        <canvas id="barChart" style="height:230px"></canvas>
                    </div>
                    @else
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            {{ trans('report.revenue_err') }}
                        </div>
                    @endif
                    <h4><i class="fa fa-bar-chart"></i> {{ trans('report.service_detail') }}</h4>
                    <!-- chi tiet tung dich vu -->
                    <table id="service-detail" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>{{ trans('report.service_name') }}</th>
                            <th>{{ trans('report.num_register') }}</th>
                            <th>{{ trans('report.num_restore') }}</th>
                            <th>{{ trans('report.num_cancel') }}</th>
                            <th>{{ trans('report.total_subscriber') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($service_detail as $key => $value)
                            <tr>
                                <td>{{ $key }}</td>
                                <td>{{ isset($value['num_register']) ? number_format($value['num_register']) : 0 }}</td>
                                <td>{{ isset($value['num_restore']) ? number_format($value['num_restore']) : 0 }}</td>
                                <td>{{ isset($value['num_cancellation']) ? number_format($value['num_cancellation']) : 0 }}</td>
                                <td>{{ isset($value['num_subscriber']) ? number_format($value['num_subscriber']) : 0 }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-md-6">
                            <h4><i class="fa fa-pie-chart"></i> {{ trans('report.subscriber_by_service') }}</h4>
                            <canvas id="pieChartRegister" style="height:250px"></canvas>
                        </div>
                        <div class="col-md-6">
                            <h4><i class="fa fa-pie-chart"></i> {{ trans('report.cancel_by_service') }}</h4>
                            <canvas id="pieChartCancel" style="height:250px"></canvas>
                        </div>
                    </div>

                </div>
            </div><!-- /.box-body -->
        </div>
    </section>
@stop

@section('custom_header')
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
@stop

@section('custom_footer')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
    <script src="{{ asset('dist/js/module/report.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#reportrange').daterangepicker(
                    {
                        ranges: {
                            '{{ trans('article.today') }}': [moment(), moment()],
                            '{{ trans('article.yesterday') }}': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            '{{ trans('article.last_7_day') }}': [moment().subtract(6, 'days'), moment()],
                            '{{ trans('article.last_30_day') }}': [moment().subtract(29, 'days'), moment()],
                            '{{ trans('article.this_month') }}': [moment().startOf('month'), moment().endOf('month')],
                            '{{ trans('article.last_month') }}': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        @if(request('start_date') != '')
                        startDate: moment('{{ date('Y-m-d H:i:s', strtotime(request('start_date'))) }}'),
                        @endif

                                @if(request('end_date') != '')
                        endDate: moment('{{ date('Y-m-d H:i:s', strtotime(request('end_date'))) }}'),
                        @endif
                    },
                    function (start, end) {
                        $('#reportrange span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
                        $('#start_date').val(start.format('YYYY-MM-DD HH:mm:ss'));
                        $('#end_date').val(end.format('YYYY-MM-DD HH:mm:ss'));
                    }
            );
                    @if(request('start_date') != '' && request('end_date') != '')
            var timeSelect = moment('{{request('start_date')}}').format('D MMMM, YYYY') + ' - ' + moment('{{ request('end_date') }}').format('D MMMM, YYYY');
            $('#time_select').html(timeSelect);
            @endif

            // init SMS Report
            ReportModule.initSMS();

            $('#service-detail').DataTable({});

            // Revenue Chart
                    @if(!empty($revenue))
            var areaChartData = {
                        labels: [
                            @foreach(array_keys($revenue) as $v)
                                    "{{ $v }}",
                            @endforeach
                        ],
                        datasets: [
                            {
                                label: "{{ trans('report.revenue_register') }}",
                                fillColor: "rgba(210, 214, 222, 1)",
                                strokeColor: "rgba(210, 214, 222, 1)",
                                pointColor: "rgba(210, 214, 222, 1)",
                                pointStrokeColor: "#c1c7d1",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(220,220,220,1)",
                                data: [
                                    @foreach($revenue as $v)
                                    {{ isset($v['REG_DAY']) ? $v['REG_DAY'] : 0}},
                                    @endforeach
                                ]
                            },
                            {
                                label: "{{ trans('report.revenue_monfee') }}",
                                fillColor: "rgba(60,141,188,0.9)",
                                strokeColor: "rgba(60,141,188,0.8)",
                                pointColor: "#3b8bba",
                                pointStrokeColor: "rgba(60,141,188,1)",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(60,141,188,1)",
                                data: [
                                    @foreach($revenue as $v)
                                    {{ isset($v['MONFEE']) ? $v['MONFEE'] : 0}},
                                    @endforeach
                                ]
                            }
                        ]
                    };

            var barChartCanvas = $("#barChart").get(0).getContext("2d");
            var barChart = new Chart(barChartCanvas);
            var barChartData = areaChartData;
            barChartData.datasets[1].fillColor = "#00a65a";
            barChartData.datasets[1].strokeColor = "#00a65a";
            barChartData.datasets[1].pointColor = "#00a65a";
            var barChartOptions = {
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - If there is a stroke on each bar
                barShowStroke: true,
                //Number - Pixel width of the bar stroke
                barStrokeWidth: 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing: 5,
                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,
                //String - A legend template
                responsive: true,
                maintainAspectRatio: true
            };

            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);
            @endif
            // Donut Register
            var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: "#fff",
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: "easeOutBounce",
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
            };
            var pieChartRegister = $("#pieChartRegister").get(0).getContext("2d");
            var pieRegister = new Chart(pieChartRegister);
            var PieDataRegister = [
                    @foreach($service_detail as $k => $v)
                {
                    value: {{ $v['num_subscriber'] }},
                    color: "{{ sprintf('#%06X', mt_rand(0, 0xFFFFFF)) }}",
                    label: "{{ $k }}"
                },
                @endforeach
            ];
            pieRegister.Doughnut(PieDataRegister, pieOptions);

            // Donut Cancel
            var pieChartCancel = $("#pieChartCancel").get(0).getContext("2d");
            var pieCancel = new Chart(pieChartCancel);
            var PieData = [
                    @foreach($service_detail as $k => $v)
                {
                    value: {{ isset($v['num_cancellation']) ? $v['num_cancellation'] : 0 }},
                    color: "{{ sprintf('#%06X', mt_rand(0, 0xFFFFFF)) }}",
                    label: "{{ $k }}"
                },
                @endforeach
            ];
            pieCancel.Doughnut(PieData, pieOptions);
        });
    </script>
@stop