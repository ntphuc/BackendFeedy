@extends('layouts.master')

@section('main_content')
<section class="content-header">
    <h1>Edit Article Review</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ trans('menu.media_zone') }}</li>
        <li class="active">{{ trans('menu.edit_recipe') }}</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <form role="form" id="recipe_form_edit" method="post">
            {{csrf_field()}}
            <div class="col-md-2">
                <ul class="nav nav-tabs-custom nav-stacked" role="tablist">
                    <li role="presentation" class="active"><a aria-controls="article_panel" role="tab"
                                                              data-toggle="tab"
                                                              href="#article_panel">
                            <i class="fa fa-bars"></i> <strong>Article</strong></a>
                    </li>
                    <li role="presentation">
                        <a aria-controls="seo_panel" role="tab" data-toggle="tab" href="#seo_panel">
                            <i class="fa fa-bars"></i> <strong>SEO Option</strong></a>
                    </li>
                    <li role="presentation"><a aria-controls="related_panel" role="tab" data-toggle="tab" href="#related_panel">
                            <i class="fa fa-bars"></i> <strong>Relate Option</strong></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-7">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="article_panel">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label><i class="fa fa-list-ul"></i> {{ trans('article.title') }}</label>
                                        <input  type="name" class="form-control" name='title' value ="{{$article->title}}"
                                                placeholder="Nhap tieu de">
                                    </div>
                                    <div class="form-group">
                                        <label><i class="fa fa-paragraph"></i> Mô tả
                                        </label>
                                        <textarea class="form-control" name="description" rows="4" id="description_article"
                                                  placeholder="{{ trans('article.description_ph') }}">{{$article->description}}</textarea>

                                        <small class="pull-right" style="margin-top: -25px;margin-right: 5px;">
                                            (words left: <span
                                                id="word_left"></span>)
                                        </small>
                                    </div>
                                    <div class="form-group">
                                        <label><i class="fa fa-paragraph"></i> Nội dung
                                        </label>
                                        <textarea class="form-control" name="content" id="editor">{!! $article->content !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label><i class="fa fa-tags"></i> {{ trans('article.tag') }}</label>
                                        <input type="name" class="form-control"
                                               placeholder="{{ trans('article.tag_ph') }}" id="tags_article" name="tags">
                                    </div>
                                    <div class="form-group">
                                        <label><i class="fa fa-tags"></i> Tin liên quan</label>
                                        <input type="name" class="form-control"
                                               placeholder="{{ trans('article.tag_ph') }}" id="tags_article" name="related">
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box-body -->
                        </div>


                    </div>
                    <div role="tabpanel" class="tab-pane" id="seo_panel">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label><i class="fa fa-list-ul"></i> {{ trans('article.seo_title') }}</label>
                                    <input type="name" class="form-control" name="seo_title" value="<?php echo isset($recipe_seo_title) ? $recipe_seo_title : '' ?>"
                                           placeholder="{{ trans('article.seo_title_ph') }}">
                                </div>
                                <div class="form-group">
                                    <label><i class="fa fa-list-ul"></i> {{ trans('article.seo_meta') }}</label>
                                    <input type="name" class="form-control" name="seo_meta"
                                           placeholder="{{ trans('article.seo_meta_ph') }}" value="<?php echo isset($recipe_seo_meta) ? $recipe_seo_meta : '' ?>">
                                </div>
                                <div class="form-group">
                                    <label><i class="fa fa-paragraph"></i> {{ trans('article.seo_description') }}
                                    </label>
                                    <textarea class="form-control" name="seo_description" rows="4" name="seo_description"
                                              placeholder="{{ trans('article.seo_description_ph') }}"><?php echo isset($recipe_seo_description) ? $recipe_seo_description : '' ?></textarea>
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    </div>





                    <div role="tabpanel" class="tab-pane" id="related_panel">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label><i class="fa fa-clock-o"></i> Thời gian chuẩn bị</label>
                                    <input  type="name" class="form-control" name='prep_time'
                                            placeholder="Th?i gian chu?n b?" value ="<?php echo isset($recipe_prep_time) ? $recipe_prep_time : '' ?>" />
                                </div>
                                <div class="form-group">
                                    <label><i class="fa fa-clock-o"></i> Thời gian nấu</label>
                                    <input  type="name" class="form-control" name='cook_time'
                                            placeholder="Th?i gian n?u" value ="<?php echo isset($recipe_cook_time) ? $recipe_cook_time : '' ?>" />
                                </div>
                                <div class="form-group">
                                    <label><i class="fa fa-balance-scale"></i> Nguyên liệu</label>
                                    <textarea class="form-control" rows="4" name="ingredients" id="infoartist"
                                              placeholder="{{ trans('article.seo_description_ph') }}"><?php echo isset($recipe_ingredients) ? $recipe_ingredients : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label><i class="fa fa-cogs"></i> C�ng th?c</label>
                                    <textarea class="form-control"  rows="4" name="directions"
                                              placeholder="{{ trans('article.seo_description_ph') }}"><?php echo isset($recipe_directions) ? $recipe_directions : '' ?></textarea>
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-image"></i>
                        <h3 class="box-title">Ảnh đại diện</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <a class="btn btn-block btn-danger fa fa-trash" style="width:35px;position: relative;top:0px ;float: right;display: none;">
                        </a>
                        <img onclick="BrowseServer('id_of_the_target_input');" src="{{$article->thumbnail}}"  id="image_replace" style="margin-top:-28px;cursor: pointer;max-height: 200px;width:100%;">
                        <div class="preview-placeholder"  id="replace" style="display:none;">
                            <div>
                                <i class="fa fa-plus fa-2x" onclick="BrowseServer('id_of_the_target_input');"></i><br>
                                <h4 class="text-muted">Bấm vào chọn ảnh</h4>
                            </div>
                        </div>

                    </div><!-- /.box-body -->
                    <input  id="id_of_the_target_input" type="hidden" name="thumbnail" value="{{$article->thumbnail}}"/>
                </div><!-- /.box -->
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-image"></i>
                        <h3 class="box-title">Tạo Gallery</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <a class="btn btn-block btn-danger fa fa-trash" style="width: 35px; position: relative; top: 0px; float: right;">
                        </a>
                        <img onclick="BrowseServer('id_of_the_target_input');" src=""  id="image_replace" style="display:none;margin-top:-28px;cursor: pointer;max-height: 200px;width:100%;">
                        <div class="preview-placeholder"  id="replace">
                            <div>
                                <i class="fa fa-plus fa-2x" onclick="BrowseServer('id_of_the_folder');"></i><br>
                                <h4 class="text-muted">Bấm vào chọn folder</h4>
                            </div>
                        </div>

                    </div><!-- /.box-body -->
                    <div id="text-folder"><?php echo isset($recipe_gallery) ? $recipe_gallery : ''; ?></div>
                    <input id="id_of_the_folder" type="hidden" class="form-control" value="" name="gallery"/>
                </div><!-- /.box -->

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-book"></i>
                        <h3 class="box-title">Chuyên mục</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="category">
                            @foreach( $category as $item )
                            <?php
                            $cat_array = array();
                            if ($article->articleCategory) {
                                foreach ($article->articleCategory as $cat_item) {
                                    $cat_array[] = $cat_item->id;
                                }
                            }
                            ?>
                            <div class="form-group category-item">
                                <label>
                                    <input @if(in_array($item->id , $cat_array))checked @endif type="checkbox" class="minimal" name="category[]" value="{{ $item->id }}"> {{ $item->title }}
                                </label>
                                <a class="primary_category" href="#setPrimaryCategory"
                                   data-url="{{ $item->id }}" data-name="{{ $item->title }}"><i
                                        class="fa fa-flag"></i></a>
                            </div>
                            @endforeach
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-book"></i>
                        <h3 class="box-title">Địa điểm</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="category">
                            @foreach( $location as $item )
                            <?php
                            $loca_array = array();
                            if ($article->articleLocation) {
                                foreach ($article->articleLocation as $loca_item) {
                                    $loca_array[] = $loca_item->id;
                                }
                            }
                            ?>
                            <div class="form-group category-item">
                                <label>
                                    <input @if(in_array($item->id , $loca_array)) checked @endif type="checkbox" class="minimal" name="location[]" value="{{ $item->id }}"> {{ $item->title }}
                                </label>
                                <a class="primary_category" href="#setPrimaryCategory"
                                   data-url="{{ $item->id }}" data-name="{{ $item->title }}"><i
                                        class="fa fa-flag"></i></a>
                            </div>
                            @endforeach
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <input name="id" id="id" type="hidden" value="{{$article->id}}" />
                <button type="submit" class="btn btn-success btn-block btn-lg"><i class="fa fa-save"></i>
                    Submit
                </button>
        </form>
    </div>
</section>
@stop

@section('custom_footer')

<script>
            $('.fa-trash').show();
            var max_len = 200;
            $(document).ready(function () {
    $.getScript("https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js");
            $.getScript("{{ asset('dist/js/module/article.js') }}");
            $("textarea[name=description]").on('keyup', function () {
    var words = 0;
            if (this.value !== '') {
    var words = this.value.match(/\S+/g).length;
            if (words > max_len) {
    // Split the string on first 200 words and rejoin on spaces
    var trimmed = $(this).val().split(/\s+/, max_len).join(" ");
            // Add a space at the end to keep new typing making new words
            $(this).val(trimmed + " ");
    }
    }
    $('#word_left').text(max_len - words);
    });
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal').iCheck({
    checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
    });
            $(".category").slimScroll({
    height: '250px'
    });
            // setPrimary hover event
            $(".category-item").hover(function () {
    // show
    $(this).find("a").show();
    }, function () {
    // hide
    var child = $(this).find("a");
            if ($(child).data('url') !== $("#inputPrimaryCateUrl").val()) {
    $(this).find("a").hide();
    }
    });
            // setPrimary func
            $(".primary_category").click(function () {
    var a = $(this).data('url');
            var b = $(this).data('name');
            $(this).css('color', 'red');
            $("#inputPrimaryCateUrl").val(a);
            $("#inputPrimaryCateName").val(b);
            // hide other cate
            $(".primary_category").not(this).css('color', '').hide();
            $(this).show();
    });
    });
            initTinyMCE("#editor", "{{url('/')}}");
            var urlobj;
            function BrowseServer(obj)
            {
            urlobj = obj;
                    OpenServerBrowser(
                            "{{url('/')}}" + '/filemanager/index.html',
                            screen.width * 0.7,
                            screen.height * 0.7);
            }
    function OpenServerBrowser(url, width, height)
    {
    var iLeft = (screen.width - width) / 2;
            var iTop = (screen.height - height) / 2;
            var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes";
            sOptions += ",width=" + width;
            sOptions += ",height=" + height;
            sOptions += ",left=" + iLeft;
            sOptions += ",top=" + iTop;
            var oWindow = window.open(url, "BrowseWindow", sOptions);
    }

    function SetUrl(url, width, height, alt)
    {
    document.getElementById(urlobj).value = url;
        if(urlobj == 'id_of_the_target_input') {
            $('#replace').hide() ;
            $('.fa-trash').show() ;
            $('#image_replace').attr('src' , url );
            $('#image_replace').show() ;
            oWindow = null;
        }else {
            $('#text-folder').html(url) ;
        }
    }
    $('input[name="tags"]').tagEditor({
    initialTags: [
            @if (!@empty($article -> tags -> meta_value))
            @foreach (json_decode($article -> tags -> meta_value) as $items)
            @if (!@empty($items))
            @foreach($items as $k => $v)
            "{{$v}}",
            @endforeach
            @endif
            @endforeach
            @endif
    ],
            autocomplete: {
            delay: 0, // show suggestions immediately
                    source: '/media/article/tag/',
                    minLength: 3,
                    placeholder: "Enter Tags In Here!",
                    position: { collision: 'flip' },
            }
    });
            $('.fa-trash').on('click', function(){
    $('#image_replace').hide();
            $('.fa-trash').hide();
            $('#replace').show();
            $('#id_of_the_target_input').attr('value', '');
    });
            $('#get_url_image').on('click', function (){
    swal({ title: "Link Image!", text: "Write Url Here:", type: "input", showCancelButton: true, closeOnConfirm: false, animation: "slide-from-top", inputPlaceholder: "Write something" },
            function(inputValue){ if (inputValue === false) return false; if (inputValue === "") { swal.showInputError("You need to write something!"); return false }
            swal({title: 'Choose Image Success', type: 'success'}, function(isConfirm){
            if (isConfirm){
            $('#replace').hide();
                    $('.fa-trash').show();
                    $('#id_of_the_target_input').attr('value', inputValue);
                    $('#image_replace').attr('src', inputValue);
                    $('#image_replace').show();
            }
            });
            });
    });
            $('.primary_category').each(function(){
    $(this).click(function(){
    var parent = $(this).parents('.form-group.category-item:first');
            var label = parent.find('label');
            var input = parent.find('.icheckbox_square-blue');
            if (input.attr('aria-checked') == 'false') {
    label.click();
    }
    });
    });
            $('input[name="related"]').tagEditor({
    initialTags: [
            @if (!@empty($article -> related -> meta_value))
            @foreach (json_decode($article -> related -> meta_value) as $items)
                @if (!@empty($items))
                @foreach($items as $k => $v)
                "{{$v}}",
                @endforeach
                @endif
            @endforeach
            @endif
    ],
            autocomplete: {
            delay: 0, // show suggestions immediately
                    source: '/media/article/related/',
                    minLength: 3,
                    maxlength : 255,
                    placeholder: "Enter Tags In Here!",
                    position: { collision: 'flip' },
            }
    });

</script>
@stop