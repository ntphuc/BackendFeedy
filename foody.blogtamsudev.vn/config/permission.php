<?php
return  [
    'ViewArticle' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ] ,
    'CreateArticle' => [
        'Admin' ,
        'Editor' ,
        'Normal'

    ] ,
    'EditArticle' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ] ,
    'ActiveArticle' =>[
        'Admin' ,
        'Editor'
    ] ,
    'DeleteArticle' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ] ,
    'PublishArticle' => [
        'Admin' ,
        'Editor'
    ],
    'ReadUser' => [
        'Admin'
    ] ,
    'ActiveUser' => [
        'Admin'
    ] ,
    'EditUser' => [
        'Admin'
    ] ,
    'DeleteUser' => [
        'Admin'
    ] ,
    'CreateUser' => [
        'Admin'
    ],
    'ViewBuilttop' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ],
    'AddBuilttop' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ],
    'PublishBuilttop' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ],
    'ChangePosition' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ],
    'DeleteBuilttop' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ],
    'SubmitChange' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ],
    'ReadCategory' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ],
    'SaveCategory' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ],
] ;