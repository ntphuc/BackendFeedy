
function deleteLocation(target) {
    var uid = $(target).data('uid');
    return swal({
        title: AdminCPLang.lang_1,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: AdminCPLang.lang_3,
        closeOnConfirm: false
    }, function () {
        $.ajax({
                type: "POST",
                url: '/media/location/delete',
                dataType: 'json',
                data: {id: uid}, // serializes the form's elements.
                success: function (res) {
                    swal({title: res.msg, type: res.status}, function () {
                        location.reload();
                    });
                }
            }
        );
    });
}