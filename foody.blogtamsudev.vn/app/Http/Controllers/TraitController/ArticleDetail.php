<?php

namespace App\Http\Controllers\TraitController;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Jobs\ReleaseContent;
use App\Models\MetaArticle;
use App\Models\Article;
use Illuminate\Support\Facades\Input;

trait ArticleDetail
{

    public function getCreate()
    {
        $this->authorize('CreateArticle');
        $data['location'] = Category::where('type', 'location')->get();
        $data['category'] = Category::where('type', 'category')->get();
        $data['guess'] = Category::where('type', 'guess')->get();
        return view('childs.article.create')->with($data);
    }

    function postCreate()
    {
        $this->authorize('CreateArticle');
        try {
            $data_article = \Input::except('_token', 'category', 'location', 'guess', 'seo_title', 'seo_meta', 'seo_description', 'tags', 'related', 'gallery', 'publish_time', 'publish_date', 'address', 'time_action', 'price');
            $meta_data = \Input::only(['address', 'time_action', 'price', 'seo_title', 'seo_meta', 'seo_description', 'category', 'location', 'guess', 'tags', 'related', 'gallery']);

            $article = new Article();
            $data = array();
            foreach ($data_article as $k => $v) {
                $article->$k = $v;
            }
            if (\Input::get('gallery') != '') {
                $folder_gallery = public_path() . \Input::get('gallery');
                if (is_dir($folder_gallery)) {
                    foreach (scandir($folder_gallery) as $k => $items) {
                        if ($k >= 3) {
                            $data[] = str_replace('//', '/', $folder_gallery . '/' . $items);
                        }
                    }
                    $gallery_path = json_encode($data);
                }else {
                    $gallery_path = '';
                }

            } else {
                $gallery_path = '';
            }
            if (Input::get('status') == '') {
                $article->status = 'Draft';
            }

            $article->slug = str_slug(\Input::get('title'));
            $article->gallery_path = $gallery_path;
            $article->creator = \Auth::user()->email;
            $article->save();


            //meta insert
            foreach ($meta_data as $k => $v) {
                if ($k == 'related' || $k == 'tags') {
                    if ($k == 'tags') {
                        $tags = explode(',', $v);
                        $meta_final = [];
                        foreach ($tags as $items) {
                            if (Category::where('title', $items)->where('type', 'tag')->count() > 0) {
                                $tag = Category::where('title', $items)->where('type', 'tag')->first();
                                $meta_final[] = [$tag->id => $tag->title];
                            } else {
                                $tag = new Category();
                                $tag->title = $items;
                                $tag->slug = str_slug($items);
                                $tag->type = 'tag';
                                $tag->parent_id = 0;
                                $tag->status = 1;
                                $tag->save();

                                $meta_final[] = [$tag->id => $items];
                            }
                        }
                    } else {
                        $meta_final = [];
                        $relates = explode(',', $v);
                        foreach ($relates as $items) {
                            $check = Article::where('title', $items)->count();
                            if ($check > 0) {
                                $relate = Article::where('title', $items)->first();
                                $meta_final[] = [$relate->id => $relate->title];
                            }
                        }
                    }
                    $meta_insert = new MetaArticle ();
                    $meta_insert->meta_key = $k;
                    $meta_insert->meta_value = json_encode($meta_final);
                    $meta_insert->article_id = $article->id;
                    $meta_insert->save();
                } elseif ($k == 'category') {
                    foreach (json_decode($v) as $cate) {
                        $meta_insert = new MetaArticle ();
                        $meta_insert->meta_key = 'relation_category';
                        $meta_insert->meta_value = $cate;
                        $meta_insert->article_id = $article->id;
                        $meta_insert->save();
                    }
                } elseif ($k == 'location') {
                    foreach (json_decode($v) as $location) {
                        $meta_insert = new MetaArticle ();
                        $meta_insert->meta_key = 'relation_location';
                        $meta_insert->meta_value = $location;
                        $meta_insert->article_id = $article->id;
                        $meta_insert->save();
                    }
                } elseif ($k == 'guess') {
                    foreach (json_decode($v) as $location) {
                        $meta_insert = new MetaArticle ();
                        $meta_insert->meta_key = 'relation_guess';
                        $meta_insert->meta_value = $location;
                        $meta_insert->article_id = $article->id;
                        $meta_insert->save();
                    }
                } else {
                    $meta_insert = new MetaArticle ();
                    $meta_insert->meta_key = 'review_' . $k;
                    $meta_insert->meta_value = $v;
                    $meta_insert->article_id = $article->id;
                    $meta_insert->save();
                }
            }


            //Job publish Article
            if (Input::get('status') == '') {
                $datetime_publish = date('Y-m-d H:i', strtotime(\Input::get('publish_date') . ' ' . \Input::get('publish_time')));
                $time_delay = (strtotime($datetime_publish) - strtotime(date('Y-m-d H:i')));
                if ($time_delay >= 0) {
                    $this->dispatch((new ReleaseContent($article->id))->delay($time_delay));
                }
            }

            //Insert Meta Seo


            return json_encode(['status' => 'success', 'msg' => 'Post Successfully']);
        } catch (\Exception $e) {
            return json_encode(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function getEdit($article_id)
    {
        $this->authorize('EditArticle');
        $data['location'] = Category::where('type', 'location')->get();
        $data['category'] = Category::where('type', 'category')->get();
        $data['guess'] = Category::where('type', 'guess')->get();
        $article = Article::find($article_id); 
        foreach ($article->articleOtherInfoReview as $v) {
            $data[$v->meta_key] = $v->meta_value;
        }

        $this->authorize('PostOfUser', $article);
        $data['article'] = $article;
        return view('childs.article.edit')->with($data);
    }

    function postEdit($article_id)
    {
        $this->authorize('EditArticle');
        try {
            $data_article = \Input::except('_token', 'category', 'location', 'guess', 'seo_title', 'seo_meta', 'seo_description', 'tags', 'related', 'gallery', 'publish_time', 'publish_date', 'address', 'time_action', 'price');
            $meta_data = \Input::only(['address', 'time_action', 'price', 'seo_title', 'seo_meta', 'seo_description', 'category', 'location', 'guess', 'tags', 'related', 'gallery']);

            $article = Article::find($article_id);

            

            $data = array();
            foreach ($data_article as $k => $v) {
                $article->$k = $v;
            }
            if (\Input::get('gallery') != '') {
                $folder_gallery = public_path() . \Input::get('gallery');
                if (is_dir($folder_gallery)) {
                    foreach (scandir($folder_gallery) as $k => $items) {
                        if ($k >= 3) {
                            $data[] = str_replace('//', '/', $folder_gallery . '/' . $items);
                        }
                    }
                    $gallery_path = json_encode($data);
                }else {
                    $gallery_path = '';
                }

            } else {
                $meta_data['gallery'] = $article->articlegallery->meta_value ;
                $gallery_path = $article->gallery_path;
            }
			$article->articleCategory()->detach();
            $article->title = \Input::get('title');
            $article->status = 'Draft';
            $article->slug = str_slug(\Input::get('title'));
            $article->gallery_path = $gallery_path;
            $article->creator = \Auth::user()->email;
            $article->save();

            //meta insert
            foreach ($meta_data as $k => $v) {
                if ($k == 'related' || $k == 'tags') {
                    if ($k == 'tags') {
                        $tags = explode(',', $v);
                        $meta_final = [];
                        foreach ($tags as $items) {
                            if (Category::where('title', $items)->where('type', 'tag')->count() > 0) {
                                $tag = Category::where('title', $items)->where('type', 'tag')->first();
                                $meta_final[] = [$tag->id => $tag->title];
                            } else {
                                $tag = new Category();
                                $tag->title = $items;
                                $tag->slug = str_slug($items);
                                $tag->type = 'tag';
                                $tag->parent_id = 0;
                                $tag->status = 1;
                                $tag->save();

                                $meta_final[] = [$tag->id => $items];
                            }
                        }
                    } else {
                        $meta_final = [];
                        $relates = explode(',', $v);
                        foreach ($relates as $items) {
                            $check = Article::where('title', $items)->count();
                            if ($check > 0) {
                                $relate = Article::where('title', $items)->first();
                                $meta_final[] = [$relate->id => $relate->title];
                            }
                        }
                    }
                    $meta_insert = new MetaArticle ();
                    $meta_insert->meta_key = $k;
                    $meta_insert->meta_value = json_encode($meta_final);
                    $meta_insert->article_id = $article->id;
                    $meta_insert->save();
                } elseif ($k == 'category') {
                    foreach (json_decode($v) as $cate) {
                        $meta_insert = new MetaArticle ();
                        $meta_insert->meta_key = 'relation_category';
                        $meta_insert->meta_value = $cate;
                        $meta_insert->article_id = $article->id;
                        $meta_insert->save();
                    }
                } elseif ($k == 'location') {
                    foreach (json_decode($v) as $location) {
                        $meta_insert = new MetaArticle ();
                        $meta_insert->meta_key = 'relation_location';
                        $meta_insert->meta_value = $location;
                        $meta_insert->article_id = $article->id;
                        $meta_insert->save();
                    }
                } elseif ($k == 'guess') {
                    foreach (json_decode($v) as $location) {
                        $meta_insert = new MetaArticle ();
                        $meta_insert->meta_key = 'relation_guess';
                        $meta_insert->meta_value = $location;
                        $meta_insert->article_id = $article->id;
                        $meta_insert->save();
                    }
                } else {
                    $meta_insert = new MetaArticle ();
                    $meta_insert->meta_key = 'review_' . $k;
                    if ($k == 'ingredients' || $k == 'directions') {
                        $meta_insert->meta_value = str_replace(PHP_EOL, '<br>', $v);
                    } else {
                        $meta_insert->meta_value = $v;
                    }
                    $meta_insert->article_id = $article->id;
                    $meta_insert->save();
                }
            }

            //Job publish Article
            $datetime_publish = date('Y-m-d H:i', strtotime(\Input::get('publish_date') . ' ' . \Input::get('publish_time')));
            $time_delay = (strtotime($datetime_publish) - strtotime(date('Y-m-d H:i')));
            if ($time_delay >= 0) {
                $this->dispatch((new ReleaseContent($article->id))->delay($time_delay));
            }
            //Insert Meta Seo


            return json_encode(['status' => 'success', 'msg' => 'Post Successfully']);
        } catch (\Exception $e) {
            return json_encode(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    function postDelete()
    {
        $this->authorize('DeleteArticle');
        try {
            $article_id = isset($_POST['id']) ? $_POST['id'] : 0;
            $article = Article::find((int)$article_id);
            $article->delete();
            return json_encode(['status' => 'success', 'msg' => 'Delete successfully']);
        } catch (\Exception $e) {
            return json_encode(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    function postPublish()
    {
        $this->authorize('PublishArticle');
        try {
            $article_id = isset($_POST['id']) ? $_POST['id'] : 0;
            $article = Article::find((int)$article_id);
            $article->status = 'publish';
            $article->save();
            return json_encode(['status' => 'success', 'msg' => 'Publish successfully']);
        } catch (\Exception $e) {
            return json_encode(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function anyEditPost(Request $request)
    {
        $this->authorize('EditArticle');
        try {
            return json_encode(['status' => 'success', 'msg' => 'Post Successfully']);
        } catch (\Exception $e) {
            return json_encode(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

}

?>